<div class="post-share-buttons">
  <a href="https://twitter.com/intent/tweet?url=<?php echo urlencode( get_permalink() ); ?>&text=<?php echo urlencode( get_the_title() ); ?>" target="_blank" title="Tweet this on Twitter" class="twitter">Share on Twitter</a>
  <a href="https://facebook.com/sharer.php?u=<?php echo urlencode( get_permalink() ); ?>&t=<?php echo urlencode( get_the_title() ); ?>" target="_blank" title="Share this on Facebook" class="facebook">Share on Facebook</a>
  <a href="https://plus.google.com/share?url=<?php echo urlencode( get_permalink() ); ?>" target="_blank" title="Post this to Google+" class="gplus">Share on Google+</a>
</div><!-- end .wpzoom-share -->