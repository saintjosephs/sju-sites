<?php 
  $showauthor = get_theme_mod( 'sjusites_showpostauthor', '1' );
  $showdate = get_theme_mod( 'sjusites_showpostdate', '1' );
  $showcategories = get_theme_mod( 'sjusites_showpostcategories', '1' );
?>
<?php 
  if(!$showauthor && !$showdate && !$showcategories):
    edit_post_link( __('Edit post'), '<p class="post-meta"><span class="edit-link">', '</span></p>', '', '' ) ;
  else:
  ?>
  
  
<p class="post-meta">
  <?php if($showauthor): ?>
    <span class="author">By <?php the_author_posts_link(); ?></span>
  <?php endif; ?>
  <?php if($showcategories): ?>
    <span class="category">In <?php the_category(', '); $prev = TRUE; ?></span>
  <?php endif; ?>
  <?php if($showdate): ?>
  <time datetime="<?php the_time("Y-m-d"); ?>" pubdate><?php the_time(get_option('date_format')); ?></time>
  <?php endif; ?>
  <span class="edit-link"><?php edit_post_link( __('Edit post', 'wpzoom'), '', ''); ?></span>
</p>
<?php endif; ?>