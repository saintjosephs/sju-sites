<?php 

$images = get_field('slider_images', 'option');

if( $images ): ?>
    <div id="sjuslider" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['sizes']['homepage-slider']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="carousel" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>


<script type="text/javascript">
  jQuery(document).ready(function() {
  
    jQuery("#sjuslider").flexslider({
      controlNav: false,
      directionNav: true,
      animationLoop: true,
      pauseOnAction: true,
      touch: false,
      animationSpeed: 500
    });	 
  
  });
</script>