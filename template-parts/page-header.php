<?php if(_sjusites_is_builder_layout() && get_post_meta( get_the_ID(), 'sjusites_layout_hidetitle', true ) == 'hidetitle'): ?>
<!-- no title because we checked "Hide Title" and we are using the page builder -->
<?php else: ?>


<header class="page-header">
  <?php if (is_archive()): ?>
    <?php 
    the_archive_title( '<h1 class="page-title">', '</h1>' );
		the_archive_description( '<div class="taxonomy-description">', '</div>' );
		?>
  <?php elseif (is_search()): ?>
    <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'sju-sites' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
  <?php elseif (is_404()): ?>
    <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sju-sites' ); ?></h1>
  <?php else: ?>
    <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
  <?php endif; ?>
</header><!-- .page-header -->
<div class="divider"></div>

<?php endif; ?>