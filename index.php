<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SJU_Sites_Theme
 */

get_header(); ?>


<?php get_template_part('template-parts/wrapper', 'top'); ?>


	<?php
		if ( have_posts() ) :
		
			if ( is_home() && ! is_front_page() ) : ?>
			
			  <?php get_template_part('template-parts','sjuslider'); ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;

			the_posts_navigation();

		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>

<?php get_template_part('template-parts/wrapper', 'bottom'); ?>
<?php get_footer(); ?>