<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SJU_Sites_Theme
 */

?>

	</div><!-- #content -->
</div><!-- #page -->


<?php get_template_part( 'sju-footer' ); ?>



<?php wp_footer(); ?>

</body>
</html>
