(function ($) {
  var resizeTimer;
  var smalllogo = $('.banner-logo img').data('small');
  var biglogo = $('.banner-logo img').attr('src');
  
  if($(window).width() < 686){
    $('.banner-logo img').attr('src',smalllogo);
  
  } else {
    $('.banner-logo img').attr('src',biglogo);
  }
  
  
  $(window).on('resize', function(e) {
    
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      if($(window).width() < 641){
        $('.banner-logo img').attr('src',smalllogo);
      
      } else {
        $('.banner-logo img').attr('src',biglogo);
      }
    }, 250);
  
  });
  
  $('.primary-menu').on('touchstart click','.btn_menu',function(e){
    e.stopPropagation();
    e.preventDefault();
    $('#menu-main').toggleClass('shown');
  });
  
  
  jQuery('#menu-main').superfish();
  
  
  
}(jQuery));