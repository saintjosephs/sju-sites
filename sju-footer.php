<footer id="footer-main">
  <div class="wrapper flex-wrap sju-footer-info">
    <div class="sju-footer-menus">
			<h3>SJU Resources</h3>
			<div class="flex-wrap">
  			<ul>
    			<li><a href="https://www.sju.edu/about-sju">About SJU</a></li>
    			<li><a href="https://www.sju.edu/about-sju/mission-statement">Mission Statement</a></li>
    			<li><a href="https://www.sju.edu/about-sju/presidents-office">President's Office</a></li>
    			<li><a href="https://www.sju.edu/about-sju/board-trustees">Board of Trustees</a></li>
    			<li><a href="https://www.sju.edu/about-sju/facts-figures">Facts and Figures</a></li>
    			<li><a href="https://jobs.sju.edu/">Employment</a></li>
    		</ul>
    		<ul>
    			<li><a href="https://www.sju.edu/campus-life">Campus Life</a></li>
    			<li><a href="https://www.sju.edu/bookstore" target="_blank">Book Store</a></li>
    			<li><a href="https://www.sju.edu/campus-life/clubs-organizations">Clubs &amp; Organizations</a></li>
    			<li><a href="https://sites.sju.edu/oid">Diversity</a></li>
<li><a href="https://www.sju.edu/library">Library</a></li>
    			<li><a href="http://www.sjuhawks.com/" target="_blank">Varsity Sports</a></li>
    		</ul>
    		<ul>
    			<li><a href="https://www.sju.edu/news">Newsroom</a></li>
    			<li><a href="https://www.sju.edu/news-events/news">News Articles</a></li>
    			<li><a href="https://www.sju.edu/news-events/magazines">Magazines</a></li>
    			<li><a href="https://www.sju.edu/news-events/information-media">Info for Media</a></li>
    		</ul>
    		<ul>
    			<li><span>Quick Links</span></li>
    			<li><a href="http://giving.sju.edu/donate" target="_blank">Giving to SJU</a></li>
    			<li><a href="https://www.sju.edu/news-events/events/academic-calendar">Academic Calendar</a></li>
    			<li><a href="https://www.sju.edu/about-sju/higher-education-opportunity-act-information">HEOA Information</a></li>
    			<li><a href="https://www.sju.edu/int/resources/emergency/">Emergency Preparedness</a></li>
    		</ul>
			</div>
    </div>
    <div class="sju-social-footer">
      <h3>Follow SJU</h3>
      <ul>
      <li><a target="_blank" href="https://www.facebook.com/saintjosephsuniversity"><i class="fa fa-facebook fa-3x">&nbsp; </i></a></li>
      <li><a target="_blank" href="https://twitter.com/saintjosephs"><i class="fa fa-twitter fa-3x">&nbsp; </i></a></li>
      <li><a target="_blank" href="https://www.youtube.com/sjuvideo"><i class="fa fa-youtube fa-3x">&nbsp; </i></a></li>
      <li><a target="_blank"  href="https://instagram.com/saintjosephs"><i class="fa fa-instagram fa-3x">&nbsp; </i></a></li>
      </ul>
    </div>
    <div class="sju-footer-ctas">
			<a class="btn btn-default btn-block" href="https://www.sju.edu/majors-programs/apply-online">Apply Online</a>
			
			<a class="btn btn-default btn-block" href="https://www.sju.edu/about-sju/visit-sju">Visit SJU</a>
		</div>
      
  </div>

  

    			

<div class="sju-address-text">
  <div class="school-name" itemscope itemtype="http://schema.org/Organization" itemprop="name">
    <strong><a itemprop="url" href="https://www.sju.edu">Saint Joseph's University</a></strong>
  </div>
  <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
    <a href="https://www.google.com/maps/place/Saint+Joseph's+University"><span itemprop="streetAddress">5600 City Ave</span>,
      <span itemprop="addressLocality">Philadelphia</span>, 
      <span itemprop="addressRegion">PA</span>
      <span itemprop="postalCode">19131</span>
    </a>
    &bull; 
    <a href="tel:16106601000">610-660-1000</a>
  </div>
</div>
			
			


		
  <div class="sil-footer">
    <div class="shadow-mid"></div>
    <div class="shadow-ground"></div>
  </div>
</footer>
