<?php


/* Post display options for the customizer */
function sjusites_register_postdisplayoptions($wp_customize) {
  $wp_customize->add_section( 'sjusites_postdisplay', array(
      'title'          => __( 'Post Display Options' ),
      'description'    => __( 'Hide or show author, date, or categories at the top of posts.  If you are not utilizing a blog / news feed for your site, you can skip these options.' ),
      'priority'       => 160
  ) );
  
  //  =============================
  //  = Checkboxes                =
  //  =============================
  $wp_customize->add_setting('sjusites_showpostauthor', array(
      'capability' => 'edit_theme_options',
      'type'       => 'theme_mod',
      'default'    => 'true'
  ));
  $wp_customize->add_control('display_sjusites_showpostauthor', array(
      'settings' => 'sjusites_showpostauthor',
      'label'    => __('Show post author in post header.'),
      'section'  => 'sjusites_postdisplay',
      'type'     => 'checkbox',
  ));
  // second
  $wp_customize->add_setting('sjusites_showpostcategories', array(
      'capability' => 'edit_theme_options',
      'type'       => 'theme_mod',
      'default'    => 'true'
      
  ));
  $wp_customize->add_control('display_sjusites_showpostcategories', array(
      'settings' => 'sjusites_showpostcategories',
      'label'    => __('Show post categories in post header.'),
      'section'  => 'sjusites_postdisplay',
      'type'     => 'checkbox',
  ));
  // third
  $wp_customize->add_setting('sjusites_showpostdate', array(
      'capability' => 'edit_theme_options',
      'type'       => 'theme_mod',
      'default'    => 'true'
  ));
  $wp_customize->add_control('display_sjusites_showpostdate', array(
      'settings' => 'sjusites_showpostdate',
      'label'    => __('Show post date in post header.'),
      'section'  => 'sjusites_postdisplay',
      'type'     => 'checkbox',
  ));
  // social share
  $wp_customize->add_setting('sjusites_show_share_buttons', array(
      'capability' => 'edit_theme_options',
      'type'       => 'theme_mod',
      'default'    => 'true'
  ));
  $wp_customize->add_control('display_sjusites_show_share_buttons', array(
      'settings' => 'sjusites_show_share_buttons',
      'label'    => __('Display sharing buttons below posts'),
      'description' => 'Check the box to enable sharing buttons (facebook, twitter, and google+) to be displayed below all blog / news posts',
      'section'  => 'sjusites_postdisplay',
      'type'     => 'checkbox',
  ));
}
add_action( 'customize_register', 'sjusites_register_postdisplayoptions' );

























function sjusites_layout_customize_register( $wp_customize ) {

  $wp_customize->add_section( 'sjusites_layout_section', array(
      'title'          => __( 'Site Layout Options' ),
      'description'    => __( 'Choose various sidebar options' ),
      'priority'       => 1
  ) );
  //Also a setting for our option
  $wp_customize->add_setting(
		'sjusites_layout',
		array(
			'default'			=> 'layout-sc',
			'type'				=> 'theme_mod',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'sjusites_layout_sanitize'
		)
	);
  
  //Now we need to build a custom class for our image radio button	
	class SJUSites_Radio_Image_Control extends WP_Customize_Control {
		public $type = 'radio-image';

		public function enqueue() {
			wp_enqueue_script( 'jquery-ui-button' );
		}
		
		public function render_content() {
			if ( empty( $this->choices ) ) {
				return;
			}			
			
			$name = '_customize-radio-' . $this->id;
			?>
			<span class="customize-control-title">
				<?php echo esc_attr( $this->label ); ?>
				<?php if ( ! empty( $this->description ) ) { ?>
					<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php } ?>
			</span>
			<div id="input_<?php echo $this->id; ?>" class="image">
				<?php foreach ( $this->choices as $value => $label ) : ?>
					<input class="image-select" type="radio" value="<?php echo esc_attr( $value ); ?>" id="<?php echo $this->id . $value; ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?>>
						<label for="<?php echo $this->id . $value; ?>">
							<img src="<?php echo esc_html( $label ); ?>" alt="<?php echo esc_attr( $value ); ?>" title="<?php echo esc_attr( $value ); ?>">
						</label>
					</input>
				<?php endforeach; ?>
			</div>
			<script>jQuery(document).ready(function($) { $( '[id="input_<?php echo $this->id; ?>"]' ).buttonset(); });</script>
			<?php
		}
	}
	//Now that we have the custom class we need to add the control itself 
	$wp_customize->add_control(
		new SJUSites_Radio_Image_Control( 
			$wp_customize,
			'sjusites_layout_control',
			array(
				'settings'		=> 'sjusites_layout',
				'section'		=> 'sjusites_layout_section',
        'type'			=> 'radio-image',
				'label'			=> __('Site Layout' ),
				'description'	=> __('Choose a layout for your site.  You can override this on a page-by-page basis.'),
				'choices'		=> array(
					'layout-scs'   => get_template_directory_uri() . '/inc/images/layout-scs.png',
    			'layout-cs' => get_template_directory_uri() . '/inc/images/layout-cs.png',
    			'layout-sc' => get_template_directory_uri() . '/inc/images/layout-sc.png',
    			// 'layout-ssc'  => get_template_directory_uri() . '/inc/images/ssc.png',
    			'layout-noside'  => get_template_directory_uri() . '/inc/images/layout-noside.png'
    		)
			)
		)
	);
            
}
add_action( 'customize_register', 'sjusites_layout_customize_register' );


//Now we need to include the sanitize callback called in the add_setting coded above
function sjusites_layout_sanitize( $value ) {
	if (!in_array( $value, array( 'layout-scs', 'layout-cs', 'layout-sc', 'layout-noside' ) ) )
	  $value = 'layout-sc';
  return $value;
}


//We will require some styling for the custom control that we added
function sjusites_radioimgpicker_custom_control_css() { 
 	?>
	<style>
	.customize-control-radio-image .image.ui-buttonset input[type=radio] {
		height: auto; 
	}
	.customize-control-radio-image .image.ui-buttonset label {
		  display: inline-block;
      margin-right: 5px;
      margin-bottom: 5px;
      margin-top: 5px;
      padding: 0;
      box-shadow: none;
      border: none;
	}
	.customize-control-radio-image .image.ui-buttonset label.ui-state-active {
		background: none;
	}
	.customize-control-radio-image .customize-control-radio-buttonset label {
		padding: 5px 10px;
		background: #f7f7f7;
		border-left: 1px solid #dedede;
		line-height: 35px; 
	}
	.customize-control-radio-image label img {
		border: 1px solid #bbb;
		opacity: 0.6;
	}
	#customize-controls .customize-control-radio-image label img {
		max-width: 50px;
		height: auto;
	}
	.customize-control-radio-image label.ui-state-active img {
		background: #dedede; 
		border-color: #000; 
		opacity: 1;
	}
	.customize-control-radio-image label.ui-state-hover img {
		opacity: 0.9;
		border-color: #999; 
	}
	.customize-control-radio-buttonset label.ui-corner-left {
		border-radius: 3px 0 0 3px;
		border-left: 0; 
	}
	.customize-control-radio-buttonset label.ui-corner-right {
		border-radius: 0 3px 3px 0; 
	}
	</style>
	<?php
}
add_action( 'customize_controls_print_styles', 'sjusites_radioimgpicker_custom_control_css' );


