<?php
/**
 * Custom functions that act independently of the theme templates.
 * @package SJU_Sites_Theme
 */


/**
*  Table of Contents
*
*   _sjusites_body_classes
*   _sjusites_is_builder_layout
*   _sjusites_get_layout
**/

/**
* _sjusites_body_classes
*
*   Adds classes to the body_class() array
*   Hooks into the wordpress body_class filter
*   
*   add_filter( 'body_class', '_sjusites_body_classes' );
*/
function _sjusites_body_classes( $classes ) {
  
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) { $classes[] = 'hfeed'; }
	
  // set the body class to the layout
	$classes[] = _sjusites_get_layout();
	
	// add a class of home to the front page
  if(!is_front_page()): $classes[] = 'not-home';
  else: $classes[] = 'home';
  endif;
	
	return $classes;
}
add_filter( 'body_class', '_sjusites_body_classes' );


/**
* _sjusites_is_builder_layout
*
*   Returns TRUE if Builder Beaver is active on the viewed page
*   
*   Example usage:
*   @code
*     if(_sjusites_is_builder_layout()):
*   @endcode
*
*/

function _sjusites_is_builder_layout(){
  if (class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_enabled()) return true;
  return false;
}


/**
* _sjusites_get_layout
*
*   Returns the layout for the page currently being viewed.
*   Defaults to the standard layout-sc (sidebar left, content right)
*   First determines if the layout is set in the customizer,
*   and then if it is overridden on the page level.
*   Returns as a string.
*   
*   Example usage:
*   @code
*     if(_sjusites_get_layout() == 'layout-noside'):
*   @endcode
*   or
*   @code
*     echo _sjusites_get_layout();
*   @endcode
*/

function _sjusites_get_layout(){
  $layout = 'layout-sc';
  
  
  if(get_post_meta( get_the_ID(), 'sjusites_post_template', true )){
    $layout = get_post_meta( get_the_ID(), 'sjusites_post_template', true );
  } else {
    $layout = get_theme_mod( 'sjusites_layout', 'layout-sc' );
  }
  
  return $layout;
}

/*************************************/

function sjusites_options_box() {
  add_meta_box('sjusites_post_layout', 'Post Layout', 'sjusites_post_layout_options', 'post', 'normal', 'high');
	add_meta_box('sjusites_post_layout', 'Page Layout', 'sjusites_post_layout_options', 'page', 'normal', 'high');
}
add_action('admin_menu', 'sjusites_options_box');


add_action('save_post', 'custom_add_save');

function custom_add_save($postID){
	// called after a post or page is saved
	if($parent_id = wp_is_post_revision($postID))
	{
	  $postID = $parent_id;
	}
  
  $hidethetitleyn = '';
  
	if (isset($_POST['save']) || isset($_POST['publish'])) {
		if (isset($_POST['sjusites_post_template'])) { update_custom_meta($postID, $_POST['sjusites_post_template'], 'sjusites_post_template'); }
		
		if (isset($_POST['sjusites_layout_hidetitle'])) $hidethetitleyn = $_POST['sjusites_layout_hidetitle'];
		update_custom_meta($postID, $hidethetitleyn, 'sjusites_layout_hidetitle');

	}
}
function update_custom_meta($postID, $newvalue, $field_name) {
	// To create new meta
	if(!get_post_meta($postID, $field_name)){
		add_post_meta($postID, $field_name, $newvalue);
	}else{
		// or to update existing meta
		update_post_meta($postID, $field_name, $newvalue);
	}

}

// Custom Post Layouts
function sjusites_post_layout_options() {
	global $post;
	$postLayouts = array('layout-scs' => 'Sidebar on both sides', 'layout-cs' => 'Sidebar on the right', 'layout-sc' => 'Sidebar on the left', 'layout-noside' => 'Full Width');
	?>

	<style type="text/css">
	    .RadioClass { display: none !important; }
	    .RadioLabelClass { margin-right: 20px;  float: left; font-weight: bold; text-align: center;}
	    img.layout-select { border: solid 2px #c0cdd6; border-radius: 3px; background: #fefefe;  }
	    .RadioSelected img.layout-select { border: solid 2px #3173b2; }
	</style>

	<script type="text/javascript">
	jQuery(document).ready(
	function($)
	{
		$(".RadioClass").change(function(){
		    if($(this).is(":checked")){
		        $(".RadioSelected:not(:checked)").removeClass("RadioSelected");
		        $(this).next("label").addClass("RadioSelected");
		    }
		});
	});
	</script>


	<fieldset>
		<div>
  		<p>The default layout for your theme is <strong>Sidebar on the left</strong>.  To change this for this page only, select one of the options below.</p>
  		<p>
			<?php
			foreach ($postLayouts as $key => $value)
			{
				?>
				<input id="<?php echo $key; ?>" type="radio" class="RadioClass" name="sjusites_post_template" value="<?php echo $key; ?>"<?php if (get_post_meta($post->ID, 'sjusites_post_template', true) == $key) { echo' checked="checked"'; } ?> />
				<label for="<?php echo $key; ?>" class="RadioLabelClass<?php if (get_post_meta($post->ID, 'sjusites_post_template', true) == $key) { echo' RadioSelected"'; } ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/inc/images/<?php echo $key; ?>.png" alt="<?php echo $value; ?>" title="<?php echo $value; ?>" class="layout-select" /></label>
			<?php
			}
			?>

			</p>
      
  		</div>
	</fieldset>
	<fieldset>
  	<p>
  	<input type="checkbox" name="sjusites_layout_hidetitle" value="hidetitle" id="sjusites_layout_hidetitle"<?php if (get_post_meta($post->ID, 'sjusites_layout_hidetitle', true) == 'hidetitle') { echo' checked="checked"'; } ?> /> Hide the title (only use this if you are editing the homepage and you are also using the pagebuilder)
  	</p>
  </fieldset><?php
}
  	




