<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SJU_Sites_Theme
 */

get_header(); ?>


<?php get_template_part('template-parts/wrapper', 'top'); ?>


	<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'single' );
      ?>
      
      
      
      <div class="divider"></div><?php
			the_post_navigation(array('prev_text' => '<i class="fa fa-angle-double-left"></i> Previous Post','next_text' => 'Next Post <i class="fa fa-angle-double-right"></i> '));
      ?>
      
      <div class="divider"></div>
      <?php if(get_theme_mod('sjusites_show_share_buttons')): ?>
      
      <?php
			get_template_part( 'template-parts/post', 'share-buttons' );
      ?><div class="divider"></div>
      
      <?php endif; ?>
      <?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

<?php get_template_part('template-parts/wrapper', 'bottom'); ?>
<?php get_footer(); ?>