<?php
/**
 * SJU Sites Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SJU_Sites_Theme
 */

/* In this file
  
  1) General setup
  2) Define Widget Areas
  3) Enqueue Scripts & Styles
  3a) Special styling override for Easy Bootstrap Pro
  4) Include additional functionality (text widget creation, post layouts, customizer additions, helper functions - see the included files for more info)
  5) Fix caption width - Strip inline width styles from WordPress's image caption shortcodes to allow for responsive images.
  6) Comments styling
  7) TinyMCE Styling
  8) Prevention of those without 'activate_plugin' (essentially, admins) from turning off beaver builder on pages where it is enabled.
  9) Let Custom Post Type UI create archives

*/


/*************************/
/* TOC: 1) General setup */
/*************************/

if ( ! function_exists( 'sju_sites_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	 function sju_sites_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SJU Sites Theme, use a find and replace
		 * to change 'sju-sites' to the name of your theme in all the template files.
		 */
	load_theme_textdomain( 'sju-sites', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for jetpack site logo.
	 */
	add_image_size( 'sjutheme-site-logo', '450', '450' );
	add_theme_support( 'site-logo', array( 'size' => 'sjutheme-site-logo' ) );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'social'	=> esc_html__( 'Social Menu', 'sju-sites' ),
		'primary' => esc_html__( 'Primary', 'sju-sites' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

  /*
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'sju_sites_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
  */
}
endif;
add_action( 'after_setup_theme', 'sju_sites_setup' );




/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sju_sites_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sju_sites_content_width', 640 );
}
add_action( 'after_setup_theme', 'sju_sites_content_width', 0 );



/*******************************/
/* TOC: 2) Define Widget Areas */
/*******************************/

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sju_sites_widgets_init() {
	register_sidebar( array(
		'name'					=> esc_html__( 'Sidebar', 'sju-theme' ),
		'id'						=> 'sidebar-1',
		'description'	 => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s" role="search" aria-label="%1$s">',
		'after_widget'	=> '</section>',
		'before_title'	=> '<h2 class="widget-title">',
		'after_title'	 => '</h2>',
	) );
	register_sidebar( array(
		'name'					=> esc_html__( 'Secondary Sidebar', 'sju-theme' ),
		'id'						=> 'sidebar-2',
		'description'	 => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s" role="menu" aria-label="%1$s">',
		'after_widget'	=> '</section>',
		'before_title'	=> '<h2 class="widget-title">',
		'after_title'	 => '</h2>',
	) );
	
	$args = array(
		'id'            => 'top_bar_widgets',
		'class'         => 'top-bar-widget-area',
		'name'          => __( 'Top Bar Widgets', 'sju-theme' ),
		'description'   => __( 'Widgets to go in the top bar of SJU (use with caution, will NOT display in mobile)', 'text_domain' ),
		'before_title'  => '',
		'after_title'   => '',
		'before_widget' => '',
		'after_widget'  => '',
	);
	register_sidebar( $args );

	$args2 = array(
		'id'            => 'header_widgets',
		'class'         => 'header-widget-area',
		'name'          => __( 'Header Area Widgets', 'sju-theme' ),
		'description'   => __( 'Widgets to go in the header area (use with caution)', 'text_domain' ),
		'before_title'  => '',
		'after_title'   => '',
		'before_widget' => '',
		'after_widget'  => '',
	);
	register_sidebar( $args2 );

}
add_action( 'widgets_init', 'sju_sites_widgets_init' );



/************************************/
/* TOC: 3) Enqueue Scripts & Styles */
/************************************/

/**
 * Enqueue scripts and styles.
 */

function sju_sites_scripts() {
  wp_enqueue_style('bootstrap_css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', false, null);
  wp_enqueue_script('bootstrap_js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array('jquery'), null, true);
  
  
	wp_enqueue_style( 'sju-sites-main', get_template_directory_uri() . '/assets/css/style.css' );
	wp_enqueue_style( 'sju-sites-style', get_stylesheet_uri() );

	wp_enqueue_script( 'sju-sites-navigation', get_template_directory_uri() . '/js/dropdown.js', array(), '20151215', true );

	wp_enqueue_script( 'sju-sites-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script('sjucustom', get_template_directory_uri() . '/js/sjucustom.js', array('jquery'), null, true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sju_sites_scripts' );



/**
 * IE enqueue flexibility with conditionals
 * @link http://tiny.cc/html5shiv
 */
function sju_sites_enqueue_flexibility()  {
  wp_enqueue_script( 'flexbox-polyfill',
      get_template_directory_uri() . '/assets/js/flexibility.js',
      array('jquery'),
      false,
      false
  );
  wp_script_add_data( 'flexbox-polyfill', 'conditional', 'lte IE 9' );
}
add_action('wp_enqueue_scripts', 'sju_sites_enqueue_flexibility');




/************************************************************/
/* TOC: 3) Enqueue Scripts & Styles 
        3a) Special styling override for Easy Bootstrap Pro */
/************************************************************/


// remove the styling for the pricingtable from the
// easy bootstrap shortcode pro, because it is making
// all our text blue.
add_action('wp_print_styles', 'sjusites_dequeue_css_from_plugins', 100);
function sjusites_dequeue_css_from_plugins()  {
	wp_dequeue_style( "bootstrap-pricingtable" ); 
}




/************************************************************/
/* TOC: 4) Include additional functionality (text widget creation, post layouts, customizer additions, helper functions - see the included files for more info)
/************************************************************/



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/textwidget.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';




/************************************************************/
/* TOC: 5) Fix caption width - Strip inline width styles from WordPress's image caption shortcodes to allow for responsive images.
/************************************************************/

/* Strip inline width styles from WordPress's image caption shortcodes to allow for responsive images.
==================================== */

add_shortcode( 'wp_caption', 'fixed_img_caption_shortcode' );
add_shortcode( 'caption', 'fixed_img_caption_shortcode' );

function fixed_img_caption_shortcode($attr, $content = null) {
	 if ( ! isset( $attr['caption'] ) ) {
		 if ( preg_match( '#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches ) ) {
		 $content = $matches[1];
		 $attr['caption'] = trim( $matches[2] );
		 }
	 }
	 $output = apply_filters( 'img_caption_shortcode', '', $attr, $content );
		 if ( $output != '' )
		 return $output;
	 extract( shortcode_atts(array(
	 'id'      => '',
	 'align'   => 'alignnone',
	 'width'   => '',
	 'caption' => ''
	 ), $attr));
	 if ( 1 > (int) $width || empty($caption) )
	 return $content;
	 if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
	 
	 
	 return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="max-width: ' . $width . 'px; width: 100%;">'
	 . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}



/************************************************************/
/* TOC: 6) Comments Template
/************************************************************/


/* Comments Custom Template
==================================== */
function wpzoom_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<div class="author-cover"><?php echo get_avatar( $comment, 40 ); ?></div>
			<?php printf( __( '%s <span class="says">says:</span>', 'wpzoom' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>

			<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php printf( __('%s at %s', 'wpzoom'), get_comment_date(), get_comment_time()); ?></a><?php edit_comment_link( __( '(Edit)', 'wpzoom' ), ' ' );
				?>

			</div><!-- .comment-meta .commentmetadata -->

		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'wpzoom' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-body">
			<?php comment_text(); ?>
			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div>
		<div class="cleaner">&nbsp;</div>

	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'wpzoom' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'wpzoom' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}

/************************************************************/
/* TOC: 7) TinyMCE Styling
/************************************************************/

/* TinyMce Styles
  http://wordpress.stackexchange.com/questions/112210/tinymce-format-dropdown-no-longer-showing-style-previews/116790#116790
==================================== */
function my_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'my_theme_add_editor_styles' );



function sju_mod_tinymce_editor( $init ){  
  unset($init['preview_styles']);
  return $init;
}
add_filter( 'tiny_mce_before_init', 'sju_mod_tinymce_editor' );

function my_mce4_options( $init ) {
$default_colours = '
	"000000", "Black"
	';
$custom_colours = '
	"9e1b32", "SJU Red"
	';
$init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';
$init['textcolor_rows'] = 1; // expand colour grid to 6 rows
return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');




/************************************************************/
/* 8) Prevention of those without 'activate_plugin' (essentially, admins) from turning off beaver builder on pages where it is enabled.
/************************************************************/

/** if you're using Beaver Builder and you're not an admin,
    STOP TURNING OFF BEAVER BUILDER  **/
    
function sju_prevent_beaver_builder_turnoff() {
  if( !current_user_can( 'activate_plugins' ) ){ 
    wp_register_style( 'custom-editor-bb-tab-hider', get_template_directory_uri() . '/custom-editor-bb-tab-hider.css', false, '1.0.0' );
    wp_enqueue_style( 'custom-editor-bb-tab-hider' );
  }
}
add_action( 'admin_init', 'sju_prevent_beaver_builder_turnoff' );







/************************************************************/
/* 9) Let Custom Post Type UI create archives
/************************************************************/


/* ======= Custom Post Types =========== */
function my_cptui_add_post_types_to_archives( $query ) {
	// We do not want unintended consequences.
	if ( is_admin() || ! $query->is_main_query() ) {
		return;    
	}

  // !! we also want to return if we are not using CPTUI
  if(!function_exists('cptui_get_post_type_slugs')){
    return;
  }
  
  // now the actual stuff.
	if ( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
		$cptui_post_types = cptui_get_post_type_slugs();

		$query->set(
			'post_type',
			array_merge(
				array( 'post' ),
				$cptui_post_types
			)
		);
	}
}
add_filter( 'pre_get_posts', 'my_cptui_add_post_types_to_archives' );