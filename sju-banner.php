<div id="sju-banner">
  <div class="wrapper">
    
    
    
    
    <?php if(is_active_sidebar( 'top_bar_widgets' )): ?>
      <div class="col-sm-3"><a href="https://www.sju.edu" class="banner-logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/signature_allwhite.png" data-small="<?php echo get_template_directory_uri(); ?>/assets/img/sju_white_50x50.gif" alt="Saint Joseph's University"></a></div>
      <div class="col-sm-9 top-bar-widgets clearfix">
      <?php dynamic_sidebar( 'top_bar_widgets' ); ?>
      </div>
    <?php else: ?>
    <a href="https://www.sju.edu" class="banner-logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/signature_allwhite.png" data-small="<?php echo get_template_directory_uri(); ?>/assets/img/sju_white_50x50.gif" alt="Saint Joseph's University"></a>
    <nav id="banner-menu" aria-label="Top SJU Menu">
      <div id="sju-menu">
        <ul>
          <li><a href="https://www.sju.edu/about-sju">ABOUT</a></li>
          <li><a href="https://nest.sju.edu">NEST</a></li>
          <li><a href="https://www.sju.edu/news">NEWS</a></li>
          <li><a href="http://alumni.sju.edu/">ALUMNI</a></li>
          <li><a href="https://www.sju.edu/university-directory">DIRECTORY</a></li>
        </ul>
      </div>
      <div id="sju-search">
        <form action="https://www.sju.edu/search" method="get" role="search" aria-label="SJU Search">
          <input class="search-input" placeholder="Search SJU" name="terms" value="">
          <button class="search-submit" type="submit"><i class="fa fa-search" title="Search SJU"></i></button>
        </form>
      </div><!-- /search -->
    </nav>
    <?php endif; ?>
  </div>
</div>