<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SJU_Sites_Theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar('sidebar-2') ) {
	return;
}
?>

<?php if(is_active_sidebar('sidebar-1')): ?>
<aside id="primary-sidebar" class="sidebar1 widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
<?php endif; ?>


<?php 
  // only show the second sidebar if we are actually
  // using a layout that supports it
  // (helps us to avoid having to do display: none on it
  $layout = get_theme_mod( 'sjusites_layout' );
  if(is_active_sidebar('sidebar-2') && ($layout == 'layout-ssc' || $layout == 'layout-scs')): ?>
<aside id="secondary-sidebar" class="sidebar2 widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-2' ); ?>
</aside><!-- #secondary -->
<?php endif; ?>

