<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SJU_Sites_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php get_template_part( 'sju-banner' ); ?>

<header id="masthead" class="site-header" role="banner">
  <div class="wrapper">
    
    <?php // two options here - the first is WITH widgets, and the second is WITHOUT.  ?>
    
    <?php if(is_active_sidebar( 'header_widgets' )): ?>
    
    <div class="site-details col-sm-8">
  		<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
  		<?php
  			$description = get_bloginfo( 'description', 'display' );
  			if ( $description || is_customize_preview() ) : ?>
  		<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
      <?php endif; ?>
    </div>
    <div class="col-sm-4 top-bar-widgets clearfix">
      <?php dynamic_sidebar( 'header_widgets' ); ?>
    </div>      
    
      
    <?php else: ?>
    <div class="site-details col-sm-8">
  		<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
  		<?php
  			$description = get_bloginfo( 'description', 'display' );
  			if ( $description || is_customize_preview() ) : ?>
  		<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
      <?php endif; ?>
    </div>
    <?php
			if ( has_nav_menu( 'social' ) ) {
				wp_nav_menu( array( 'theme_location' => 'social', 'depth' => 1, 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'container_class' => 'social-links' ) );
			}
		?>
    <?php endif; ?>
  </div>
</header>

<div id="nav-main-menu" class="primary-menu clearfix">
  <div class="wrapper primary-menu">
			<?php
				/*
				 * Aria Label: Provides a label to differentiate multiple navigation landmarks
				 * hidden heading: provides navigational structure to site for scanning with screen reader
				 */
			?>
			<nav id="site-navigation" class="main-navigation" aria-label='<?php _e( 'Primary Menu ', 'sju-sites' ); ?>'>
  			<a class="btn_menu" id="toggle" href="#"><i class="fa fa-bars fa-1x"></i></a>
				<?php if (has_nav_menu( 'primary' )) {
					wp_nav_menu( array(
						'container' => '',
						'container_class' => '',
						'menu_class' => 'nav navbar-nav dropdown sf-menu',
						'menu_id' => 'menu-main',
						'sort_column' => 'menu_order',
						'theme_location' => 'primary',
						'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<li class="cleaner">&nbsp;</li></ul>'
					) );
				}
				?>
			</nav>
  </div>
</div>

	
<div id="content" class="site-content wrapper">
